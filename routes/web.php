<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', 'PageController@getTest')->name('test');

Route::get('/', 'PageController@getHome')->name('homePage');
Route::post('/newsletter', 'PageController@storeNewsletter')->name('newsletter');

Route::get('/help', 'PageController@getHowTo')->name('howToPage');
Route::get('/productShop/{tag?}', 'PageController@getProducts')->name('productsPage');
Route::get('/about', 'PageController@getAbout')->name('aboutPage');
Route::get('/terms', 'PageController@getTerms')->name('termsPage');
Route::get('/policy', 'PageController@getPolicy')->name('policyPage');

//Route::get('/contact', 'PageController@getContact')->name('contactPage');
Route::get('contact', 'ContactController@show')->name('contact.show');
Route::post('contact','ContactController@create')->name('contact.create');

Route::get('/profile/{user}', 'UserProfileController@showProfile')->name('user.profile');
Route::put('/profile/{user}', 'UserProfileController@updateUserProfile')->name('user.profile.update');
Route::put('/profileaddress/{user}', 'UserProfileController@updateUserAddress')->name('user.address.update');
Route::put('/profilepassword/{user}', 'UserProfileController@updateUserPassword')->name('user.password.update');

Route::get('/orderHistory', 'OrderController@index')->name('order.index');
Route::post('/order', 'OrderController@store')->name('order.store');

//socialite routes
//Route::get('login/google', 'Auth\LoginController@redirectToProvider')->name('googleLogin');
//Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/editor/{product?}', 'PageController@editor')->name('editorPage');
Route::get('searchResult', 'PageController@getSearchProduct')->name('productSearch');
Route::post('/preview', 'PageController@editorPreview')->name('editorPreview');

Route::middleware('role:admin')->prefix('admin')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/dashboard', 'AdminController@getDashboard')->name('admin.dashboard');

        Route::resource('categories', 'CategoryController');
        Route::resource('tags', 'TagController');
        Route::resource('products', 'ProductController');
        Route::resource('templateImages', 'TemplateImageController');

        Route::get('/orders', 'AdminOrderController@getOrders')->name('admin.orders');
        Route::get('/orders/{customerOrder}', 'AdminOrderController@updateOrderStatus')->name('admin.orders.update');
        Route::get('/items/{customerOrder}', 'AdminOrderController@viewOrderItems')->name('admin.order.items');

        Route::get('/featured', 'PageController@getFeaturedProduct')->name('featured.product');
        Route::get('/featured/{product}', 'PageController@removeFeaturedProduct')->name('update.featured.product');
    });
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/myCart', 'CartController@index')->name('cart.index');
    Route::post('/myCart/{product}', 'CartController@store')->name('cart.store');
    Route::delete('/myCart/{cartitem}', 'CartController@delete')->name('cart.destroy');
    Route::put('/myCart/{cartitem}', 'CartController@update')->name('cart.update');
});