{{--@extends('layouts.app')--}}

{{--@section('content')--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">Login</div>--}}

                    {{--<div class="panel-body">--}}
                        {{--<form class="form-horizontal" method="POST" action="{{ route('login') }}">--}}
                            {{--{{ csrf_field() }}--}}

                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="email" type="email" class="form-control" name="email"--}}
                                           {{--value="{{ old('email') }}" required autofocus>--}}

                                    {{--@if ($errors->has('email'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                                {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-6 col-md-offset-4">--}}
                                    {{--<div class="checkbox">--}}
                                        {{--<label>--}}
                                            {{--<input type="checkbox"--}}
                                                   {{--name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-8 col-md-offset-4">--}}
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                        {{--Login--}}
                                    {{--</button>--}}

                                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                        {{--Forgot Your Password?--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endsection--}}

@extends('layout.appNoFooter')

@section('content')
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h1 class="page_title mx-auto d-table">LOGIN</h1>
                <form class="custom-form mt-4" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control custom-form_input" name="email"
                               placeholder="Enter email" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control custom-form_input" name="password"
                               placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div> -->
                    <button type="submit" class="btn btn-block custom-form_button mt-5">LOG IN</button>
                    {{--<a href="{{ route('password.request') }}" class="custom-form_link mt-4 d-block">Forgot Password?</a>--}}

                    <p class="text-center mt-2">- or -</p>

                    {{--<button class="btn btn-block custom-form_button--facebook d-flex align-items-center justify-content-center mt-3">--}}
                        {{--<span class="mr-4">Connect with Facebook</span>--}}

                        {{--<svg xmlns="http://www.w3.org/2000/svg" height="18px" fill="#fff" viewBox="6765 496 8.364 18">--}}
                            {{--<path id="facebook_f" data-name="facebook f" class="cls-1"--}}
                                  {{--d="M26.346,11.924H23.5V10.056a.761.761,0,0,1,.793-.865H26.3V6.107L23.532,6.1a3.506,3.506,0,0,0-3.773,3.773v2.056H17.982V15.1h1.777V24.1H23.5V15.1H26.02Z"--}}
                                  {{--transform="translate(6747.018 489.904)"/>--}}
                        {{--</svg>--}}
                    {{--</button>--}}

                    <a  href="{{ url('login/google') }}" class="btn btn-block custom-form_button--google d-flex align-items-center justify-content-center mt-3">

                        <svg enable-background="new 0 0 50 50" id="Layer_1" height="28px" version="1.1" viewBox="0 0 50 50"
                             xml:space="preserve" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path
                                    d="M45,1H5C2.8,1,1,2.8,1,5v40c0,2.2,1.8,4,4,4h40c2.2,0,4-1.8,4-4V5C49,2.8,47.2,1,45,1z"
                                    fill="#FFFFFF"/>
                            <g>
                                <path d="M20.3,10.5c-3.7,1.3-6.9,4.1-8.7,7.6c-0.6,1.2-1.1,2.5-1.3,3.9c-0.7,3.3-0.2,6.9,1.3,9.9   c1,2,2.4,3.7,4.2,5.1c1.6,1.3,3.5,2.3,5.6,2.8c2.6,0.7,5.3,0.7,7.8,0.1c2.3-0.5,4.5-1.6,6.3-3.3c1.9-1.7,3.2-3.9,3.9-6.4   c0.8-2.6,0.9-5.4,0.4-8.1c-4.8,0-9.6,0-14.4,0c0,2,0,4,0,6c2.8,0,5.6,0,8.3,0c-0.3,1.9-1.5,3.7-3.1,4.7c-1,0.7-2.2,1.1-3.4,1.3   c-1.2,0.2-2.5,0.2-3.7,0c-1.2-0.2-2.4-0.8-3.4-1.5c-1.6-1.1-2.9-2.8-3.5-4.7c-0.7-1.9-0.7-4,0-6c0.5-1.3,1.2-2.6,2.2-3.6   c1.2-1.3,2.8-2.2,4.6-2.5c1.5-0.3,3-0.3,4.5,0.2c1.2,0.4,2.4,1.1,3.3,1.9c0.9-0.9,1.9-1.9,2.8-2.8c0.5-0.5,1-1,1.5-1.5   c-1.4-1.3-3.1-2.4-4.9-3.1C27.3,9.4,23.6,9.4,20.3,10.5z"
                                      fill="#FFFFFF"/>
                                <g>
                                    <path d="M20.3,10.5c3.3-1.1,7-1.1,10.3,0.1c1.8,0.7,3.5,1.7,4.9,3.1c-0.5,0.5-1,1-1.5,1.5    c-0.9,0.9-1.9,1.9-2.8,2.8c-0.9-0.9-2.1-1.6-3.3-1.9c-1.4-0.4-3-0.5-4.5-0.2c-1.7,0.4-3.3,1.3-4.6,2.5c-1,1-1.8,2.3-2.2,3.6    c-1.7-1.3-3.3-2.6-5-3.9C13.4,14.6,16.6,11.8,20.3,10.5z"
                                          fill="#EA4335"/>
                                </g>
                                <g>
                                    <path d="M10.3,22c0.3-1.3,0.7-2.6,1.3-3.9c1.7,1.3,3.3,2.6,5,3.9c-0.7,1.9-0.7,4,0,6c-1.7,1.3-3.3,2.6-5,3.9    C10.1,28.8,9.6,25.3,10.3,22z"
                                          fill="#FBBC05"/>
                                </g>
                                <g>
                                    <path d="M25.3,22.1c4.8,0,9.6,0,14.4,0c0.5,2.7,0.4,5.5-0.4,8.1c-0.7,2.4-2,4.6-3.9,6.4c-1.6-1.3-3.2-2.5-4.9-3.8    c1.6-1.1,2.7-2.8,3.1-4.7c-2.8,0-5.6,0-8.3,0C25.3,26.1,25.3,24.1,25.3,22.1z"
                                          fill="#4285F4"/>
                                </g>
                                <g>
                                    <path d="M11.6,31.9c1.7-1.3,3.3-2.6,5-3.9c0.6,1.9,1.9,3.6,3.5,4.7c1,0.7,2.2,1.2,3.4,1.5c1.2,0.2,2.4,0.2,3.7,0    c1.2-0.2,2.4-0.7,3.4-1.3c1.6,1.3,3.2,2.5,4.9,3.8c-1.8,1.6-3.9,2.7-6.3,3.3c-2.6,0.6-5.3,0.6-7.8-0.1c-2-0.5-3.9-1.5-5.6-2.8    C14.1,35.6,12.6,33.8,11.6,31.9z"
                                          fill="#34A853"/>
                                </g>
                            </g>
                        </svg>
                        <span class="ml-2">SIGN IN WITH GOOGLE</span>
                    </a>


                    <p class="custom-form_link--account mt-4 d-block text-center">
                        New to Lambada? <a href="{{ route('register') }}">REGISTER</a>
                    </p>
                </form>
            </div>

        </div>

    </div>
@endsection
