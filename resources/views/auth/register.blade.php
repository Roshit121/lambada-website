@extends('layout.appNoFooter')

@section('content')
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h1 class="page_title mx-auto d-table">REGISTER</h1>
                <form class="custom-form mt-4" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="name" type="text" class="form-control custom-form_input" name="name"
                               placeholder="Enter Full Name" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control custom-form_input" name="email"
                               placeholder="Enter Email" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control custom-form_input" name="password"
                               placeholder="Password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control custom-form_input" name="password_confirmation"
                               placeholder="Confirm Password" required>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <input id="phone" type="text" class="form-control custom-form_input" name="phone"
                               placeholder="Phone" required>

                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<input type="phone" class="form-control custom-form_input" id="phoneNumber"--}}
                               {{--placeholder="Phone Number" required>--}}
                    {{--</div>--}}

                    <button type="submit" class="btn btn-block custom-form_button mt-5">REGISTER</button>

                    <a href="{{ route('login') }}" class="custom-form_link--account mt-4 d-block text-center">Back to Login
                        Page</a>
                </form>
            </div>

        </div>
    </div>
@endsection
