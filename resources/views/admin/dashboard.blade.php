@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-icon text-center bg-warning">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="stat-text">Total Customers</div>
                        <div class="stat-digit">
                            {{ $customers }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card">
                <div class="stat-icon text-center bg-info">
                    <i class="fas fa-cubes"></i>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="stat-text">Total Products</div>
                        <div class="stat-digit">
                            {{ $products }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card">
                <div class="stat-icon text-center bg-danger">
                    <i class="fas fa-truck-moving"></i>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="stat-text">Incomplete Orders</div>
                        <div class="stat-digit">
                            {{ $orders }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card">
                <div class="stat-icon text-center bg-success">
                    <i class="fas fa-hand-holding-usd"></i>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="stat-text">Total Earnings</div>
                        <div class="stat-digit">
                            Rs. {{ $earnings[0]->total_sales }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection