@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Template Image</h1>
        <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#addTemplateImage">
            <i class="fas fa-plus mr-2"></i>
            Add Template
        </button>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($templateimages as $templateimage)
                            <tr>
                                <td class="align-middle">{{ $templateimage->id }}</td>
                                <td class="align-middle">{{ $templateimage->name }}</td>
                                <td>
                                    <img src="{{asset($templateimage->image->url)}}" height="100px">
                                </td>
                                <td class="align-middle">
                                    {{ Form::open(['route' => ['templateImages.destroy', $templateimage->id], 'method' => 'DELETE']) }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addTemplateImage" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Template Image</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => route('templateImages.store'),'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                    {{ Form::label('name', 'Name:') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                    <label for="template_image">Template Image</label>
                    <input type="file" class="form-control" name="template_image">
                    {{ Form::submit('Add Template Image', ['class' => 'btn btn-primary btn-block mt-4']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection