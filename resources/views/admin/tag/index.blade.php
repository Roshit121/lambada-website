@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Tags</h1>
        <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#addtag">
            <i class="fas fa-plus mr-2"></i>
            Add Tag
        </button>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($tags as $tag)
                            <tr>
                                <td>{{ $tag->id }}</td>
                                <td>{{ $tag->name }}</td>
                                <td>
                                    <button type="button" class="btn btn-primary mr-3 d-inline-block" data-toggle="modal"
                                            data-target="#updatetag-{{ $tag->id }}">Edit
                                    </button>

                                    {{ Form::open(['route' => ['tags.destroy', $tag->id], 'method' => 'DELETE', 'class'=>'d-inline-block']) }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            <div class="modal fade" id="updatetag-{{ $tag->id }}" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Tag</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            {{ Form::model($tag, ['route' => ['tags.update', $tag->slug], 'method' => "PUT"]) }}
                                            {{ Form::label('name', "Title:") }}
                                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                                            {{ Form::submit('Save Changes', ['class' => 'btn btn-primary btn-block', 'style' => 'margin-top:20px;']) }}
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addtag" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Tag</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'tags.store', 'method' => 'POST']) !!}
                    {{ Form::label('name', 'Name:') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                    {{ Form::submit('Create New Tag', ['class' => 'btn btn-primary btn-block mt-4']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection