@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Orders</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Customer Name</th>
                            <th>Address</th>
                            <th>Contact Number</th>
                            <th>Charge</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->user->name }}</td>
                                {{--<td><a href="{{ route('categories.show', $order->id) }}">{{ $category->name }}</a></td>--}}
                                {{--<td>{{ $category->name }}</td>--}}
                                <td>{{ $order->user->address }}</td>
                                <td>{{ $order->user->phone }}</td>
                                <td>{{ $order->charge }}</td>
                                <td>{{ $order->status }}</td>
                                <td>
                                    <a href="{{route('admin.orders.update',$order->id)}}" type="button" class="btn btn-success">
                                        Completed
                                    </a>
                                    <a href="{{route('admin.order.items', $order->id)}}" type="button" class="btn btn-info">
                                        View More
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection