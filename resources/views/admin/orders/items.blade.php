@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Order Items</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>Product Code</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item)
                            <tr>
                                {{--<td>{{ $item->product_id }}</td>--}}
                                <td class="align-middle">{{ $item->product()->get()->first()->product_code }}</td>
                                <td class="align-middle">{{ $item->name }}</td>
                                <td class="align-middle">{{ $item->price }}</td>
                                <td class="align-middle">{{ $item->quantity }}</td>
                                <td>
                                    <img src="{{asset($item->imageURI)}}" class="table-image">
                                </td>
                                <td class="align-middle">
                                    <a href="{{ $item->imageURI }}" download="{{asset($item->imageURI)}}" class="btn btn-primary" type="button">
                                        Download
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection