<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/dashboard') ? "active" : "" }}" href="{{route('admin.dashboard')}}">
                    <span data-feather="layout"></span>
                    Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/products') ? "active" : "" }}" href="{{route('products.index')}}">
                    <span data-feather="package"></span>
                    Product
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/orders') ? "active" : "" }}" href="{{route('admin.orders')}}">
                    <span data-feather="shopping-bag"></span>
                Orders
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/featured') ? "active" : "" }}" href="{{route('featured.product')}}">
                    <span data-feather="award"></span>
                    Featured Product
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/templateImages') ? "active" : "" }}" href="{{route('templateImages.index')}}">
                    <span data-feather="clipboard"></span>
                    Template
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/categories') ? "active" : "" }}" href="{{route('categories.index')}}">
                    <span data-feather="grid"></span>
                    Category
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('admin/tags') ? "active" : "" }}" href="{{route('tags.index')}}">
                    <span data-feather="tag"></span>
                    Tag
                </a>
            </li>

        </ul>
    </div>
</nav>