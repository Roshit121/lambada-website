@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Featured Products</h1>

    </div>

    {{--{{dd($featureMovies)}}--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="content table-responsive table-full-width">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>Product Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($featuredProducts as $featuredProduct)
                            <tr>
                                <td> {{ $featuredProduct->name}}</td>
                                <td><a href="{{route('update.featured.product',$featuredProduct->slug)}}" type="button" class="btn btn-danger">Remove</a></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection