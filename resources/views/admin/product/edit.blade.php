@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Product</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{ Form::model($product,['url' => route( 'products.update',$product->product_code),'method'=>'PUT','enctype'=>'multipart/form-data']) }}

                {{--{{ Form::model($category, ['route' => ['categories.update', $category->slug], 'method' => "PUT"]) }}--}}
                {{--{{dd($movie)}}--}}
                <div class="col-sm-12">
                    {{ Form::label('product_code', 'Product Code') }}
                    {{ Form::text('product_code', null, ["class" => 'form-control']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::label('name', 'Product Name') }}
                    {{ Form::text('name', null, ["class" => 'form-control']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::label('featured_product', 'Feature this Product') }}
                    {{ Form::checkbox('featured_product','1',null, ["class" => 'form-control']) }}
                    {{--<label for="feature_movie">Feature this Movie</label>--}}
                    {{--<input type="checkbox" name="feature_movie" value="1"><br>--}}
                </div>

                <div class="col-sm-12">
                    {{ Form::label('price', 'Price') }}
                    {{ Form::text('price', null, ["class" => 'form-control']) }}
                </div>

                <div class="col-sm-12">
                    {{ Form::label('discount', 'Discount') }}
                    {{ Form::text('discount', null, ["class" => 'form-control']) }}
                </div>

                <div class="col-sm-12">
                    {{ Form::label('category_id', 'Product Category') }}
                    {{ Form::select('category_id', $selectedcategory, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::label('tags', 'Tags') }}
                    {{ Form::select('tags[]', $selectedtag, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple']) }}
                </div>

                {{--<div class="col-sm-12">--}}
                    {{--{{ Form::label('first_image', 'First Image') }}--}}
                    {{--{{ Form::file('first_image', null, ["class" => 'form-control']) }}--}}
                {{--</div>--}}

                {{--<div class="col-sm-12">--}}
                    {{--{{ Form::label('second_image', 'Second Image') }}--}}
                    {{--{{ Form::file('second_image', null, ["class" => 'form-control']) }}--}}
                {{--</div>--}}

                {{--<div class="col-sm-12">--}}
                    {{--{{ Form::label('third_image', 'Third Image') }}--}}
                    {{--{{ Form::file('third_image', null, ["class" => 'form-control']) }}--}}
                {{--</div>--}}

                {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/editor-style.css') }}">
    {!! Html::style('css/select2.min.css')!!}
@endpush
@push('scripts')
    <script src="{{ asset('js/fabric.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    {!! Html::script('js/select2.min.js')!!}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2-multi').select2();
        });
    </script>
@endpush