@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Product</h1>
        <a href="{{route('products.create')}}">
            <button type="button" class="btn btn-info pull-right">
                <i class="fas fa-plus mr-2"></i>
                Add Product
            </button>
        </a>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>Product Code</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td class="align-middle">{{ $product->product_code }}</td>
                                <td class="align-middle">{{ $product->name }}</td>
                                <td class="align-middle">
                                    <img src="{{asset($product->image->url)}}" height="150px">
                                </td>

                                <td class="align-middle">
                                    <a href="{{route('products.edit',$product->product_code)}}" type="button" class="btn btn-primary mr-3 d-inline-block">Edit</a>

                                    {{ Form::open(['route' => ['products.destroy', $product->id], 'method' => 'DELETE', 'class'=>'d-inline-block']) }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                    {{ Form::close() }}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection