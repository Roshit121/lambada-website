@extends('admin.layout.app')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Add Product</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{--{{ dd($templates) }}--}}
                {{--@foreach($templates as $template)--}}
                    {{--<img src="{{asset($template->image->url)}}">--}}
                {{--@endforeach--}}
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['url' => route('products.store'),'method'=>'POST','enctype'=>'multipart/form-data', 'class'=>'mb-5']) !!}
                <div class="col-sm-12">
                    <label for="product_code">Product Code</label>
                    <input type="text" class="form-control" name="product_code">
                </div>
                <div class="col-sm-12">
                    <label for="name">Product Name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="col-sm-12">
                    <input type="checkbox" name="featured_product" value="1"><br>
                    <label for="featured_product d-inline-block">Feature this Product</label>
                </div>
                <div class="col-sm-12">
                    <label for="price">Price</label>
                    <input type="text" class="form-control" name="price">
                </div>
                <div class="col-sm-12">
                    <label for="discount">Discount</label>
                    <input type="text" class="form-control" name="discount">
                </div>
                <div class="col-sm-12">
                    <label for="category_id">Product Category</label>
                    <select class="form-control" name="category_id">
                        @foreach($categories as $category)
                            <option value='{{ $category->id }}'>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-12">
                    <label for="tags[]">Tag</label>
                    <select class="form-control select2-multi" name="tags[]" multiple="multiple">
                        @foreach($tags as $tag)
                            <option value='{{ $tag->id }}'>{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-12">
                    <label for="first_image">First Image</label>
                    <input type="file" class="form-control" name="first_image">
                </div>
                <div class="col-sm-12">
                    <label for="second_image">Second Image</label>
                    <input type="file" class="form-control" name="second_image">
                </div>
                <div class="col-sm-12">
                    <label for="third_image">Third Image</label>
                    <input type="file" class="form-control" name="third_image">
                </div>

                <div class="col-sm-12">
                    {{--<label for="data">Product Data</label>--}}
                    {{--<textarea class="form-control" name="data"></textarea>--}}

                    <div id="app">
                        <admin-editor></admin-editor>
                        {{--<admin-editor images="{!! json_encode($templateArray) !!}"></admin-editor>--}}
                    </div>
                </div>

                <br><br>
                {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/editor-style.css') }}">
    {!! Html::style('css/select2.min.css')!!}
@endpush
@push('scripts')
    <script>
        var images = JSON.parse("{!! addslashes(json_encode($templateArray)) !!}");
    </script>
    <script src="{{ asset('js/fabric.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    {!! Html::script('js/select2.min.js')!!}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2-multi').select2();
        });
    </script>
@endpush