<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylesheet.css')}}">

    <title>Lambada | Home</title>

</head>

<body>
<!-- home banner -->
<nav class="navbar navigation-bar navbar-expand-md navbar-dark navbar-inverse" id="navigation_on_banner">
    <div class="col-md-8 offset-md-2">
        <div class="row">
            <a href="#" class="navbar-brand">
                <img src="images/icons/logo.svg" alt="logo" id="logo-white">
                <img src="images/icons/logo-footer.svg" class="brand_image--hide" alt="logo" id="logo">
            </a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#mainNav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="mainNav">
                <div class="navbar-nav w-100 align-items-center">
                    <div class="input-group d-flex align-self-center justify-content-center invisible"
                         id="occasion_selection">
                        <select class="occasion_select" id="occasion_selector-navigation">
                            <option>Select Occasion</option>
                            @if($tags)
                                @foreach($tags as $tag)
                                    <option value="{{ route('productsPage', $tag->slug) }}">{{ $tag->name }}</option>
                                @endforeach
                            @endif

                        </select>
                        <div class="input-group-append">
                            <a type="button" herf="#" class="btn occasion_select--button" id="occasion_search-navigation">GO</a>
                        </div>
                    </div>

                    <a class="navbar-item nav-link {{Request::is('/') ? "active" : "" }}" href="{{route('homePage')}}">Home</a>
                    <a class="navbar-item nav-link {{Request::is('products') ? "active" : "" }}"
                       href="{{route('productsPage')}}">Shop</a>
                    <a class="navbar-item nav-link {{Request::is('about') ? "active" : "" }}"
                       href="{{route('aboutPage')}}">About</a>
                    <a class="navbar-item nav-link {{Request::is('contact') ? "active" : "" }}"
                       href="{{route('contact.show')}}">Contact</a>

                    <a class="navbar-item nav-link ml-4 d-flex align-items-center" data-toggle="collapse" href="#searchSection" role="button">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="791 176 13 13" class="header_icons">
                            <g id="search" transform="translate(791 176)">
                                <circle id="Ellipse_4" data-name="Ellipse 4" class="cls-2" cx="4.16" cy="4.16" r="4.16"
                                        transform="translate(1.3 1.04)"/>
                                <line id="Line_1" data-name="Line 1" class="cls-3" x2="3.45" y2="3.45"
                                      transform="translate(8.38 8.38)"/>
                            </g>
                        </svg>
                    </a>

                    {{--<a class="navbar-item nav-link" href="{{ route('login') }}">--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="1270 176 13 13" class="header_icons">--}}

                            {{--<g id="user" transform="translate(1270 176)">--}}
                                {{--<path id="Path_2" data-name="Path 2" class="cls-2"--}}
                                      {{--d="M13.223,11.707a10.747,10.747,0,0,1-.023-1.095,2.142,2.142,0,0,0,.587-1.271c.149-.012.385-.158.453-.733a.537.537,0,0,0-.2-.537c.243-.73.747-2.99-.933-3.223a1.321,1.321,0,0,0-1.191-.457c-2.3.042-2.58,1.738-2.075,3.681a.538.538,0,0,0-.2.537c.069.575.3.72.453.733a2.138,2.138,0,0,0,.6,1.271,10.75,10.75,0,0,1-.024,1.095c-.456,1.225-3.529.881-3.671,3.243h9.88C16.738,12.587,13.678,12.931,13.223,11.707Z"--}}
                                      {{--transform="translate(-5.18 -3.249)"/>--}}
                            {{--</g>--}}
                        {{--</svg>--}}

                    {{--</a>--}}

                    {{--cart--}}
                    <a class="navbar-item nav-link position-relative d-flex align-items-center" href="{{ route('cart.index') }}">
                    {{--<a class="navbar-item nav-link" href="#">--}}
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="1313 176 13 13.24" class="header_icons">
                            <g id="cart" transform="translate(1313 176)">
                                <circle id="Ellipse_1" data-name="Ellipse 1" class="cls-2" cx="0.78" cy="0.78" r="0.78"
                                        transform="translate(9.36 11.18)"
                                />
                                <circle id="Ellipse_2" data-name="Ellipse 2" class="cls-2" cx="0.78" cy="0.78" r="0.78"
                                        transform="translate(5.98 11.18)"
                                />
                                <circle id="Ellipse_3" data-name="Ellipse 3" cx="0.52" cy="0.52" r="0.52"
                                        transform="translate(0 0.52)"/>
                                <path id="Path_3" data-name="Path 3" class="cls-3"
                                      d="M12.14,13.4H8.164c-.889,0-1.02-.453-1.176-1.047L4.91,3.961C4.723,3.331,4.459,3,3.733,3H2"
                                      transform="translate(-1.48 -2.22)"/>
                                <path id="Path_4" data-name="Path 4" class="cls-3" d="M16,16h8.06l-1.87,4.94H17.2"
                                      transform="translate(-11.84 -11.84)"/>
                            </g>
                        </svg>
                        <span class="badge cart-badge">
                            {{ Cart::count() }}
                        </span>
                    </a>

                    @guest
                        {{--profile--}}
                        <a class="navbar-item nav-link d-flex align-items-center" href="{{ route('login') }}" style="white-space: nowrap">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="1270 176 13 13" class="header_icons ml-4 mr-2">

                                <g id="user" transform="translate(1270 176)">
                                    <path id="Path_2" data-name="Path 2" class="cls-2" d="M13.223,11.707a10.747,10.747,0,0,1-.023-1.095,2.142,2.142,0,0,0,.587-1.271c.149-.012.385-.158.453-.733a.537.537,0,0,0-.2-.537c.243-.73.747-2.99-.933-3.223a1.321,1.321,0,0,0-1.191-.457c-2.3.042-2.58,1.738-2.075,3.681a.538.538,0,0,0-.2.537c.069.575.3.72.453.733a2.138,2.138,0,0,0,.6,1.271,10.75,10.75,0,0,1-.024,1.095c-.456,1.225-3.529.881-3.671,3.243h9.88C16.738,12.587,13.678,12.931,13.223,11.707Z"
                                          transform="translate(-5.18 -3.249)" />
                                </g>
                            </svg>
                            Login
                        </a>
                    @else
                        <li class="nav-item dropdown ml-4">
                            <a class="nav-link dropdown-toggle d-flex align-items-center" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ explode(' ', trim(Auth::user()->name))[0] }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('user.profile', auth()->user()->id) }}">Profile</a>
                                <a class="dropdown-item" href="{{ route('cart.index', auth()->user()->id) }}">My Cart</a>
                                <a class="dropdown-item" href="{{ route('order.index', auth()->user()->id) }}">Order History</a>
                                @role('admin')
                                <a class="dropdown-item" href="{{ route('admin.dashboard', auth()->user()->id) }}">Dashboard</a>
                                @endrole
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endguest
                </div>
                <!-- end of navbar-nav -->
            </div>
            <!-- end of collapse -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of column -->
</nav>
<div class="search_bar collapse" id="searchSection">
    <div class="container">
        {{--<input type="text" class="form-control search_bar--input" name="" value="">--}}
        {{--<button class="btn btn-primary" data-toggle="collapse" data-target="#searchSection">X</button>--}}
        <div class="input-group justify-content-center align-items-center">
            <form action="{{route('productSearch')}}" method="get" class="form-inline ml-5">
                <input type="text" name="search" class="form-control search_bar--input" placeholder="Search for...">
                <div class="input-group-append">
                    <button class="btn search_button--search" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="791 176 13 13"
                             class="search_button--search-icon">
                            <g id="search" transform="translate(791 176)">
                                <circle id="Ellipse_4" data-name="Ellipse 4" class="cls-2" cx="4.16" cy="4.16" r="4.16"
                                        transform="translate(1.3 1.04)"/>
                                <line id="Line_1" data-name="Line 1" class="cls-3" x2="3.45" y2="3.45"
                                      transform="translate(8.38 8.38)"/>
                            </g>
                        </svg>
                    </button>
                </div>
            </form>
            <button type="button" class="btn search_button--close ml-4" data-toggle="collapse"
                    data-target="#searchSection">
                <span>&times;</span>
            </button>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row landing-page_banner-image">
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="landing-page_banner-title">
                            <p class="display-2">
                                <span class="font-weight-bold">DESIGN</span>
                            </p>
                            <P class="landing-page_banner-title--sub">YOUR OWN
                                <span class="font-weight-bold">PRODUCT</span>
                            </P>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control landing-page_selector" id="occasion_selector">
                            <option>Select Occasion</option>
                            @if($tags)
                                @foreach($tags as $tag)
                                    <option value="{{ route('productsPage', $tag->slug) }}">{{ $tag->name }}</option>
                                @endforeach
                            @endif
                            {{--<option>Dashain</option>--}}
                            {{--<option>Tihar</option>--}}
                            {{--<option>Holi</option>--}}
                            {{--<option>Christmas</option>--}}
                            {{--<option>New Year</option>--}}
                        </select>
                        <a type="button" href="#" class="btn landing-page_search-button mt-4 float-right" id="occasion_search">SEARCH</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- offer -->
<div class="container mt-5">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h1 class="section_title">OFFER</h1>

            <div class="row mt-4">
                <div class="col-md-4 offer_item">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="offer_item--container">
                                <img src="images/card.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="offer_item--container container-red">
                                <div class="offer_item--content text-right">
                                    <h5>
                                        <small>START</small>
                                    </h5>
                                    <h3 class="mb-3">
                                        <b>DESIGNING</b>
                                    </h3>
                                    <a href="{{ route('editorPage', 'MG01') }}" class="offer-button">DESIGN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 offer_item">
                    <div class="offer_item--container offer_item--container_height2">
                        <img src="images/pexels-photo-175936.jpg" alt="">
                        <div class="offer_item--overlay"></div>
                        <div class="offer_item--content text-right">
                            <h2>BIG</h2>
                            <h2>OFFER</h2>
                            <h1>-{{ $discounts->first()->discount }}%</h1>
                            {{--<button class="btn offer_item--content_buy-button mt-4">BUY</button>--}}
                            <a class="btn offer_item--content_buy-button mt-4" type="button" href="{{ route('editorPage', $discounts->first()->product_code) }}">Buy</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 offer_item">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="offer_item--container container-dark">
                                <div class="offer_item--content text-right">
                                    <h2>
                                        <b>-30%</b>
                                    </h2>
                                    <h5 class="mb-3">
                                        <small>ON MOTHER'S DAY</small>
                                    </h5>

                                    <a href="{{ route('productsPage', 'Mother\'s Day') }}" class="offer-button">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="offer_item--container">
                                <img src="images/card2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- mug carousel -->
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-md-12 carousel-slanted">
            <div class="carousel-slanted_background">

            </div>
            <div class="col-md-5 carousel-container text-center">
                <div class="carousel-item" id="carousel_home">
                    <div class="carousel-slide">
                        <img id="slider-image" src="images/1.jpg" alt="">
                    </div>
                    <div class="carousel-slide">
                        <img id="slider-image" src="images/2.jpg" alt="">
                    </div>
                    <div class="carousel-slide">
                        <img id="slider-image" src="images/3.jpg" alt="">
                    </div>
                </div>
                <img src="images/mug.png" class="home_mockup-mug" alt="">
                <img src="images/mockup-shadow.png" class="home_mockup-shadow " alt="">
            </div>
        </div>
    </div>
</div>

<!-- featured products -->
<div class="container featured">
    <div class="row">
        <div class="col-md-12">
            <h1 class="section_title">FEATURED PRODUCTS</h1>

            <div class="row mt-4 text-center">
                @if($featuredProducts)
                @foreach($featuredProducts as $featuredProduct)
                        <div class="col-md-3">
                            <a href="#" class="product-container--link">
                                <div class="product-container">
                                    <div class="products-slider mx-auto text-center">
                                        <div>
                                            @if($image = $featuredProduct->image()->where('meta','first_image')->first())
                                                <img src="{{asset($image->url) }}" class="img-fluid mx-auto">
                                            @endif
                                        </div>
                                        <div>
                                            @if($image = $featuredProduct->image()->where('meta','second_image')->first())
                                                <img src="{{asset($image->url) }}" class="img-fluid mx-auto">
                                            @endif
                                        </div>
                                        <div>
                                            @if($image = $featuredProduct->image()->where('meta','third_image')->first())
                                                <img src="{{asset($image->url) }}" class="img-fluid mx-auto">
                                            @endif
                                        </div>
                                    </div>
                                    <p class="product-name mt-4">{{ $featuredProduct->name }}</p>
                                    <p class="product-price">Rs. {{ $featuredProduct->price }}</p>
                                    <a class="btn btn-block product_button mt-4 mx-auto" type="button" href="{{ route('editorPage', $featuredProduct->product_code) }}">Personalise</a>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif


            </div>
        </div>
    </div>
</div>

<!-- footer -->
<div class="container-fluid mt-5 footer">
    <div class="row footer-top">
        <div class="col-md-8 offset-md-2">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 footer-top_logo">
                            <img src="../images/icons/logo-footer.svg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 footer-top_contact--details mt-4 d-flex">
                            <img src="../images/icons/phone.svg" class="d-inline-block align-self-center" alt="">
                            <p class="ml-3 d-inline-block">+977 9803531145</p>
                        </div>
                        <div class="col-md-12 footer-top_contact--details d-flex">
                            <img src="../images/icons/mail.svg" class="d-inline-block align-self-center" alt="">
                            <p class="ml-3 d-inline-block">lambada@gmail.com</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mt-4 footer-top_social-links">
                            <a href="#" class="d-inline-block">
                                <img src="../images/icons/faceboook.svg" alt="">
                            </a>
                            <a href="#" class="d-inline-block ml-2">
                                <img src="../images/icons/instagram.svg" alt="">
                            </a>
                            <a href="#" class="d-inline-block ml-2">
                                <img src="../images/icons/twitter.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <h4 class="footer-top_links--heading">USEFUL LINKS</h4>
                    <ul class="footer-top_links mt-2">
                        <li>
                            <a href="{{route('termsPage')}}">Terms & condition</a>
                        </li>
                        <li>
                            <a href="{{route('policyPage')}}">Privacy Policies</a>
                        </li>
                        <li>
                            <a href="{{route('aboutPage')}}">About Us</a>
                        </li>
                        <li>
                            <a href="{{route('contact.show')}}">Contact Us</a>
                        </li>
                        <li>
                            <a href="{{route('howToPage')}}">How To Use</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4">
                    <h4 class="footer-top_links--heading">MY ACCOUNT</h4>
                    <ul class="footer-top_links mt-2">
                        @guest
                            <li>
                                <a href="{{route('login')}}">Login</a>
                            </li>
                            <li>
                                <a href="{{route('register')}}">Register</a>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('user.profile', auth()->user()->id) }}">Profile</a>
                            </li>
                            <li>
                                <a href="{{ route('cart.index', auth()->user()->id) }}">View Cart</a>
                            </li>
                            <li>
                                <a href="{{ route('order.index', auth()->user()->id) }}">Order History</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @if (session('status'))
        <div class="alter alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row footer-middle">
        <div class="col-md-4 offset-md-2 mt-5 newletters">
            <h4 class="newletters_heading">NEWLETTERS</h4>
            <form action="{{ route('newsletter') }}" method="POST">
                <div class="input-group mt-3 newletters_email">
                    <input type="email" name="user_email" class="form-control newletters_email--input" placeholder="Email">
                    <div class="input-group-append">
                        {{ csrf_field() }}
                        <button class="btn newletters_email--button" type="submit">
                            <svg class="mb-1" xmlns="http://www.w3.org/2000/svg" viewBox="637.193 3195.2 28.891 12"
                                 height="9px">
                                <path id="input-arrow" class="cls-1"
                                      d="M8.891,11a.567.567,0,0,0-.16-.389l-5.4-5.45a.541.541,0,0,0-.771,0,.554.554,0,0,0,0,.778l4.465,4.51H-19.455A.548.548,0,0,0-20,11a.548.548,0,0,0,.545.55H7.03l-4.465,4.51a.554.554,0,0,0,0,.778.541.541,0,0,0,.771,0l5.4-5.45A.564.564,0,0,0,8.891,11Z"
                                      transform="translate(657.193 3190.2)"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4 newletters_img--container">
            <img src="../images/mockUpCollection.png" class="img-fluid pull-right" alt="">
        </div>
    </div>

    <div class="row footer-bot">
        <div class="col-md-8 offset-md-2 text-center align-self-center">
            <small>&copy; 2018 Roshit Shrestha. All rights reserved</small>
        </div>
    </div>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('js/jquery.slim.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('slick/slick.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script>
    $(document).ready(function () {
        $('#occasion_selector').on('change', function () {
            var tag = $(this).val();
            console.log(tag)
            $('#occasion_search').attr('href', tag);
        });
        $('#occasion_selector-navigation').on('change', function () {
            var tag = $(this).val();
            console.log(tag)
            $('#occasion_search-navigation').attr('href', tag);
        });
    });
</script>
</body>

</html>
