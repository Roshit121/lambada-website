@extends('layout.app')
@section('title')
    About Us
@endsection
@section('content')
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h1 class="page_title mx-auto d-table">ABOUT US</h1>
                <div class="mt-4 about_image-container">
                    <img src="../images/banner.jpg" class="img-fluid about_image" alt="">
                </div>
                <p class="mt-4 page_paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel orci ligula. Nullam lacinia felis
                    tortor, id ornare nisi eleifend varius. Proin arcu risus, bibendum vitae feugiat sit amet, ornare ut
                    est. Duis pharetra eget lorem eu tincidunt. Maecenas quis orci hendrerit, luctus enim tempus, dapibus
                    justo. Phasellus in ante id diam sollicitudin semper. Nulla id eleifend ante. Morbi a cursus massa, pharetra
                    euismod quam. Curabitur feugiat vel urna non condimentum.</p>
                <p class="mt-4 page_paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel orci ligula. Nullam lacinia felis
                    tortor, id ornare nisi eleifend varius. Proin arcu risus, bibendum vitae feugiat sit amet, ornare ut
                    est. Duis pharetra eget lorem eu tincidunt. Maecenas quis orci hendrerit, luctus enim tempus, dapibus
                    justo. Phasellus in ante id diam sollicitudin semper. Nulla id eleifend ante. Morbi a cursus massa, pharetra
                    euismod quam. Curabitur feugiat vel urna non condimentum.</p>

            </div>

        </div>
    </div>
@endsection