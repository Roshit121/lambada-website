@extends('layout.app')
@section('title')
    Privacy Policies
@endsection
@section('content')
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h1 class="page_title mx-auto d-table">PRIVACY POLICIES</h1>
                <p class="mt-4 page_paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel orci ligula. Nullam lacinia felis
                    tortor, id ornare nisi eleifend varius. Proin arcu risus, bibendum vitae feugiat sit amet, ornare ut
                    est. Duis pharetra eget lorem eu tincidunt. Maecenas quis orci hendrerit, luctus enim tempus, dapibus
                    justo. Phasellus in ante id diam sollicitudin semper. Nulla id eleifend ante. Morbi a cursus massa, pharetra
                    euismod quam. Curabitur feugiat vel urna non condimentum.</p>

                <h3 class="page_sub-title mt-5">INTRODUCTION</h3>
                <p class="mt-3 page_paragraph">
                    Sed auctor semper ex, nec commodo nisi consequat sit amet. Nam laoreet felis ac diam vehicula, pharetra tincidunt velit tristique.
                    Praesent sed lacus eu purus fringilla porttitor. Donec vestibulum facilisis diam sed rhoncus. Suspendisse
                    condimentum ligula elit, eu porta sapien sagittis eget. Aliquam eleifend, justo in vulputate consectetur,
                    mauris dolor consequat tellus, et ultricies sem massa at velit. Mauris viverra ipsum id sem gravida pulvinar.
                    Duis tempor lectus ac sapien gravida efficitur. Nam mi lorem, imperdiet nec nisi eget, convallis blandit
                    ligula. Nullam turpis erat, tristique ut nisl eu, euismod ornare est.
                </p>
                <p class="mt-3 page_paragraph">
                    Duis non mollis sem, quis bibendum tellus. Aliquam quis ante ex. Vivamus imperdiet ullamcorper dui, vel rhoncus turpis tempus
                    vel. Nam mattis rutrum ligula, quis imperdiet quam pretium sit amet. Cras in blandit nisi. Aliquam mauris
                    ligula, bibendum quis odio et, ultrices maximus orci. Etiam nec metus dapibus, vulputate eros vel, imperdiet
                    metus. Donec tempor convallis libero vel scelerisque. Proin in libero quis quam euismod viverra eget
                    sit amet lacus. Donec lobortis dolor magna, ut commodo velit convallis eu.
                </p>

                <h3 class="page_sub-title mt-5">POLICIES</h3>
                <p class="mt-3 page_paragraph">
                    Nam lacinia et leo et aliquam. Nam dictum feugiat elit, et tempor lorem iaculis sed. Aliquam erat volutpat. Aenean euismod
                    maximus lorem, molestie bibendum purus porttitor id. Vestibulum tincidunt, eros sit amet tincidunt rutrum,
                    libero enim consectetur sapien, nec semper metus lorem vitae orci. Cras quis tincidunt massa. Suspendisse
                    potenti. Proin aliquet lacus tellus, sit amet luctus odio iaculis ornare. Aenean vehicula maximus dui,
                    a rhoncus mi tincidunt a. Maecenas consectetur tortor sed lacus accumsan, bibendum luctus lorem imperdiet.
                    Phasellus malesuada, nulla ac mattis ultricies, augue mi tincidunt ante, sed convallis nibh ex id nunc.

                </p>
                <p class="mt-3 page_paragraph">
                    Praesent viverra congue nibh, quis sollicitudin purus posuere et. Aliquam erat volutpat. Ut ut mi non erat imperdiet accumsan.
                    Fusce pretium mauris ut metus porta, at porttitor nisl hendrerit. Mauris sed ante id est tincidunt eleifend.
                    Vivamus in leo id ligula vestibulum ornare. Ut consectetur enim nec arcu volutpat fringilla. Cras ut
                    lectus non purus ullamcorper hendrerit. Integer dictum magna in nunc malesuada suscipit. Suspendisse
                    scelerisque in nulla in pulvinar. Aenean vitae ipsum massa. Donec eget lorem eget libero placerat auctor.
                    Nulla in purus finibus, mollis augue at, pharetra velit.
                </p>

            </div>

        </div>
    </div>
@endsection