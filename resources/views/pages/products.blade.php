@extends('layout.app')
@section('title')
    Products
@endsection
@section('content')
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                {{--<div class="row">--}}
                    {{--<h1 class="page_title">--}}
{{--                        {{ dd($tag->name) }}--}}

                        {{--@if(is_null($tag->name))--}}
                            {{--Shop--}}
                        {{--@else--}}
                            {{--{{ $tag->name }}--}}
                        {{--@endif--}}
                    {{--</h1>--}}
                    {{--<div>--}}
                    {{--@foreach($tag->products()->get() as $product)--}}
                    {{--<p>{{$product->name}}</p><br>--}}
                    {{--@endforeach--}}
                    {{--</div>--}}

                    {{--<select class="ml-auto product_filter" id="">--}}
                        {{--<option selected>Filter by</option>--}}
                        {{--<option value="popular">Popular</option>--}}
                        {{--<option value="newest">Newest</option>--}}
                        {{--<option value="priceHighLow">Price high to low</option>--}}
                        {{--<option value="priceLowHigh">Price low to high</option>--}}
                    {{--</select>--}}
                {{--</div>--}}

                <div class="row mt-4">
                    <div class="col-md-3 pl-0 position-relative">
                        <div class="product_title_container">
                            <span class="background-text">MUG</span>
                            <h1 class="product_page_title">
                                @if(is_null($tag->name))
                                    Shop
                                @else
                                    {{ $tag->name }}
                                @endif
                            </h1>
                        </div>
                                {{--<p class="our_products">Our Products</p>--}}
                                {{--<div class="product_type">--}}
                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="all">--}}
                                        {{--<label class="custom-control-label" for="all">All</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="cards">--}}
                                        {{--<label class="custom-control-label" for="cards">Cards</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="mugs">--}}
                                        {{--<label class="custom-control-label" for="mugs">Mugs</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="custom-control custom-checkbox">--}}
                                        {{--<input type="checkbox" class="custom-control-input" id="tshirts">--}}
                                        {{--<label class="custom-control-label" for="tshirts">T-Shirts</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    @if($products)
                                        {{--@foreach($products as $product)--}}
                                @if(is_null($tag->name))
                                    @foreach($products as $product)
                                        <div class="col-md-4 mb-4">
                                            <a href="#" class="product-container--link">
                                                <div class="product-container">
                                                    <div class="products-slider mx-auto text-center">
                                                        {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                                                        {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                                                        {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                                                        <div>
                                                            @if($image = $product->image()->where('meta','first_image')->first())
                                                                <img src="{{asset($image->url) }}"
                                                                     class="img-fluid mx-auto">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            @if($image = $product->image()->where('meta','second_image')->first())
                                                                <img src="{{asset($image->url) }}"
                                                                     class="img-fluid mx-auto">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            @if($image = $product->image()->where('meta','third_image')->first())
                                                                <img src="{{asset($image->url) }}"
                                                                     class="img-fluid mx-auto">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    {{--<p class="product-name mt-4">Mug Name</p>--}}
                                                    {{--<p class="product-price">Rs. 350</p>--}}
                                                    <p class="product-name mt-4">{{ $product->name }}</p>
                                                    <p class="product-price">Rs. {{ $product->price }}</p>
                                                    <a class="btn btn-block product_button mt-4 mx-auto" type="button"
                                                       href="{{ route('editorPage', $product->product_code) }}">Personalise</a>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                @else
                                    @foreach($tag->products()->get() as $product)
                                        <div class="col-md-4 mb-4">
                                            <a href="#" class="product-container--link">
                                                <div class="product-container">
                                                    <div class="products-slider mx-auto text-center">
                                                        <div>
                                                            @if($image = $product->image()->where('meta','first_image')->first())
                                                                <img src="{{asset($image->url) }}"
                                                                     class="img-fluid mx-auto">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            @if($image = $product->image()->where('meta','second_image')->first())
                                                                <img src="{{asset($image->url) }}"
                                                                     class="img-fluid mx-auto">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            @if($image = $product->image()->where('meta','third_image')->first())
                                                                <img src="{{asset($image->url) }}"
                                                                     class="img-fluid mx-auto">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    {{--<p class="product-name mt-4">Mug Name</p>--}}
                                                    {{--<p class="product-price">Rs. 350</p>--}}
                                                    <p class="product-name mt-4">{{ $product->name }}</p>
                                                    <p class="product-price">Rs. {{ $product->price }}</p>
                                                    <a class="btn btn-block product_button mt-4 mx-auto" type="button"
                                                       href="{{ route('editorPage', $product->product_code) }}">Personalise</a>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            @endif

                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 mb-4">--}}
                            {{--<a href="#" class="product-container--link">--}}
                            {{--<div class="product-container">--}}
                            {{--<div class="products-slider mx-auto text-center">--}}
                            {{--<div><img src="../images/mug1/mug-front.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-mid.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--<div><img src="../images/mug1/mug-back.png" alt="" srcset="" class="img-fluid mx-auto"></div>--}}
                            {{--</div>--}}
                            {{--<p class="product-name mt-4">Mug Name</p>--}}
                            {{--<p class="product-price">Rs. 350</p>--}}
                            {{--<button class="btn btn-block product_button mt-4 mx-auto">Personalise</button>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script src="../slick/slick.min.js"></script>
    @endpush
@endsection