@extends('layout.appNoFooter')
@section('content')
    <div class="container-fluid mt-4 min-main-height">
        {{--<div class="row">--}}
            {{--<div class="col-md-4 offset-md-4">--}}
                {{--<h1 class="page_title mx-auto d-table">Editor</h1>--}}
                    <div id="app">
                        <editor token="{{ csrf_token() }}" previewurl="{{ route('editorPreview') }}"
                                addtocarturl="{{ route('cart.store',$product->product_code) }}"></editor>
                    </div>
            {{--</div>--}}
        {{--</div>--}}


        {{--<img src="{{asset('images/preview_image/wrap1.png')}}"  alt="">--}}
    </div>

@endsection

@push('scripts')
    <script>
        var template = JSON.parse('{!! $product->data !!}');
    </script>

    <script src="{{asset('slick/slick.min.js')}}"></script>
    <script src="{{ asset('js/fabric.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@endpush