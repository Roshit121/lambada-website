@extends('layout.appNoFooter')
@section('title')
    Contact Us
@endsection
@section('content')
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <h1 class="page_title mx-auto d-table">CONTACT US</h1>
                <form class="contact-form mt-4" action="{{ url('contact') }}" method="POST">
                    {{ csrf_field() }}
                    {{--<div class="form-group">--}}
                    {{--<!-- <label for="inputEmail">Email address</label> -->--}}
                    {{--<input type="text" class="form-control contact-form_input" id="fullName" placeholder="Enter Full Name">--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <input type="email" class="form-control contact-form_input" id="email" name="email" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control contact-form_input" id="subject" name="subject" placeholder="Enter Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control contact-form_textarea" id="message" name="message" placeholder="Message" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-block contact-form_button mt-4">SEND</button>
                </form>

            </div>

        </div>
    </div>
@endsection