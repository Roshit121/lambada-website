<h3>A customer placed an order</h3>

<div>
    {{ $name }} has placed {{ $ordernumber }} numbers of unique orders.
</div>

<p>Sent via {{ $email }}</p>