<!doctype html>
<html lang="en">
@include('partial._head')

<body>
@include('partial._nav')
@yield('content')
@include('partial._script')
@stack('scripts')
</body>
</html>