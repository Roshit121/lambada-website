<!doctype html>
<html lang="en">
@include('partial._head')

<body>
@include('partial._nav', ['item'=>'1'])
@yield('content')
@include('partial._footer')
@include('partial._script')
@stack('scripts')
</body>
</html>