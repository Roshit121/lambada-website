<div class="col-md-2 pl-0">
    <ul class="profile_side-navigation">
        <li><a href="{{ route('user.profile', auth()->user()->id) }}"
               class="{{ Request::is('profile') ? "active" : "" }}">Profile</a></li>
        <li><a href="{{ route('cart.index', auth()->user()->id) }}" class="{{ Request::is('myCart') ? "active" : "" }}">My
                Cart</a></li>
        <li><a href="{{ route('order.index', auth()->user()->id) }}"
               class="{{ Request::is('orderHistory') ? "active" : "" }}">Order History</a></li>
        <li>
            {{--<a href="">Sign-out</a>--}}
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                Sign-out
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</div>