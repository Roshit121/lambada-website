@extends('layout.app')
@section('content')
{{--    {{dd($cartitems)}}--}}
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="row">
                    <h1 class="page_title">YOUR PROFILE</h1>
                </div>

                <div class="row mt-4">
                    @include('profile.profileSidebar')
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <h3 class="page_sub-title">MY CART</h3>
                            </div>
                            <div class="col-md-12">
                                {{--{{dd(auth()->user()->id)}}--}}
                                @if($cartitems)
                                        <table class="table table-hover order-history_table cart-table">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">PRODUCT NAME</th>
                                                <th scope="col">PRICE</th>
                                                <th scope="col">QUANTITY</th>
                                                <th scope="col">IMAGE</th>
                                                <th scope="col">ACTION</th>
                                            </tr>
                                            </thead>
                                            <tbody class="table-with-image">
                                            @foreach($cartitems as $cartitem)
                                                <tr>
                                                    <td>{{ $cartitem -> name }}</td>
                                                    <td>{{ $cartitem -> price }}</td>
                                                    <td>
                                                        <div class="btn-group ">
                                                            <button type="button" class="btn btn-light dropdown-toggle quantity-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                {{ $cartitem -> qty }}
                                                            </button>
                                                            <div class="dropdown-menu p-4">
                                                                {{ Form::open( ['route' => ['cart.update', $cartitem->rowId], 'method' => 'PUT', 'style' => 'width:max-content; width: -moz-max-content']) }}
                                                                {{ Form::label('name', "Quantity:") }}
                                                                <div class="input-group">
                                                                    {{ Form::text('name', null, ['class' => 'form-control', 'style' => 'width:100px;']) }}
                                                                    <div class="input-group-append">
                                                                        {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                                                                    </div>
                                                                </div>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><img src="{{ $cartitem -> options -> imageURI }}" alt=""
                                                             height="100px"></td>
                                                    <td>
                                                        {{--<a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{ route('cart.destroy', $cartitem->rowId) }}">Remove</a>--}}
                                                        {{ Form::open(['route' => ['cart.destroy', $cartitem->rowId], 'method' => 'DELETE', 'onsubmit'=> 'return confirm("Are you sure?")']) }}
                                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                                        {{ Form::close() }}
                                                    </td>
                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    <span class="lead">Total Amount: {{ $carttotal }}</span>
                                    {{--<input type="text" value="{{ $finalPrice }}" name="finalPrice">--}}
                                    <br />

                                    <div class="row mt-4">
                                        <div class="col-md-4 offset-md-4">
                                            {!! Form::open(['url' => route('order.store'),'method'=>'POST']) !!}
                                            {{ Form::button('<i class="fas fa-check-circle mr-2"></i>Order', ['class' => 'btn btn-success btn-block btn-lg', 'type' => 'submit']) }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                @else
                                    You have no Item in your cart
                                @endif

                            </div>

                            {{--<div class="col-md-12 d-flex justify-content-center mt-5">--}}
                                {{--<button class="btn keep-shopping_button">KEEP ON SHOPPING</button>--}}
                            {{--</div>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection