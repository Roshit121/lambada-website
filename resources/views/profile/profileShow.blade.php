@extends('layout.app')
@section('content')
    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="row">
                    <h1 class="page_title">YOUR PROFILE</h1>
                </div>

                <div class="row mt-4">
                    @include('profile.profileSidebar')
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <h3 class="page_sub-title">PROFILE</h3>
                            </div>

                            @if (session('addressCheck'))
                                <div class="col-md-12">
                                    <div class="alert alert-warning" role="alert">
                                        {{ session('addressCheck') }}
                                    </div>
                                </div>
                            @endif
                            @if(session()->has('warning'))
                                <div class="col-md-12">
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        {!! session()->get('warning') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if(session()->has('success'))
                                <div class="col-md-12">
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {!! session()->get('success') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            @endif


                            <div class="col-md-6 pr-3">
                                <div class="basic_information">
                                    <h4 class="component_title d-inline-block mb-1">BASIC INFORMATION</h4>
                                    <a href="" class="d-inline-block float-right" data-toggle="modal" data-target="#basicInformationEdit">Edit</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="basicInformationEdit" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="d-flex justify-content-end">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="d-flex my-4">
                                                        <div class="modal-form">
                                                            {{ Form::model($user, ['route' => ['user.profile.update', $user->id], 'method' => "PUT"]) }}
                                                                <div class="form-group row">
                                                                    <label for="fullName" class="col-md-3 offset-md-1 col-form-label modal-form_label">Name</label>
                                                                    <div class="col-md-7">
                                                                        {{ Form::text('name', null, ['class' => 'form-control modal-form_input']) }}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="email" class="col-md-3 offset-md-1 col-form-label modal-form_label">Email</label>
                                                                    <div class="col-md-7">
                                                                        {{ Form::text('email', null, ['class' => 'form-control modal-form_input']) }}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="phone" class="col-md-3 offset-md-1 col-form-label modal-form_label">Phone Number</label>
                                                                    <div class="col-md-7">
                                                                        {{ Form::text('phone', null, ['class' => 'form-control modal-form_input']) }}
                                                                    </div>
                                                                </div>
                                                            <div class="row">
                                                                <div class="col-md-10 offset-md-1 d-flex justify-content-end">
                                                                    {{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
                                                                </div>
                                                            </div>
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <p class="profile_information-title">Full Name</p>
                                    <p class="profile_information">{{ $user->name }}</p>

                                    {{--<p class="profile_information-title">Last Name</p>--}}
                                    {{--<p class="profile_information">Shrestha</p>--}}

                                    <p class="profile_information-title">E-mail</p>
                                    <p class="profile_information">{{ $user->email }}</p>

                                    <p class="profile_information-title">Phone Number</p>
                                    <p class="profile_information">{{ $user->phone }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 pl-3">
                                <div class="row d-flex">
                                    <div class="col-md-12 mb-3">
                                        <div class="change_password">
                                            <h4 class="component_title d-inline-block mb-2">CHANGE PASSWORD</h4>
                                            <a href="" class="d-inline-block float-right" data-toggle="modal" data-target="#passwordEdit">Edit</a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="passwordEdit" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <div class="d-flex justify-content-end">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>

                                                            <div class="d-flex my-4">
                                                                <div class="modal-form">
                                                                    {{ Form::model($user, ['route' => ['user.password.update', $user->id], 'method' => "PUT"]) }}
                                                                    <div class="form-group row">
                                                                        <label for="oldPassword" class="col-md-3 offset-md-1 col-form-label modal-form_label">Old Password</label>
                                                                        <div class="col-md-7">
                                                                            <input type="password" class="form-control modal-form_input" name="current_password">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="newPassword" class="col-md-3 offset-md-1 col-form-label modal-form_label">New Password</label>
                                                                        <div class="col-md-7">
                                                                            <input type="password" class="form-control modal-form_input" name="password">
                                                                        </div>
                                                                    </div>
                                                                    {{--<div class="form-group row">--}}
                                                                        {{--<label for="confirmPassword" class="col-md-3 offset-md-1 col-form-label modal-form_label">Confirm Password</label>--}}
                                                                        {{--<div class="col-md-7">--}}
                                                                            {{--<input type="password" class="form-control modal-form_input" id="confirmPassword">--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="row">--}}
                                                                        {{--<div class="col-md-11 text-right">--}}
                                                                            {{--<button type="button" class="btn btn-outline-secondary modal-form_button mr-2" data-dismiss="modal">Close</button>--}}
                                                                            {{--<button type="button" class="btn btn-primary modal-form_button">Save changes</button>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    <div class="row">
                                                                        <div class="col-md-10 offset-md-1 d-flex justify-content-end">
                                                                            {{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
                                                                        </div>
                                                                    </div>
                                                                    {{ Form::close() }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <p class="profile_information">Change your account's password</p>
                                        </div>
                                    </div>
                                    <div class="col-md-12 align-self-end mt-3">
                                        <div class="primary_address">
                                            <h4 class="component_title d-inline-block mb-2">PRIMARY ADDRESS</h4>
                                            <a href="" class="d-inline-block float-right" data-toggle="modal" data-target="#addressEdit">Edit</a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="addressEdit" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <div class="d-flex justify-content-end">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>

                                                            <div class="d-flex my-4">
                                                                <div class="modal-form">
                                                                    {{ Form::model($user, ['route' => ['user.address.update', $user->id], 'method' => "PUT"]) }}
                                                                        <div class="form-group row">
                                                                            <label for="primaryAddress" class="col-md-3 offset-md-1 col-form-label modal-form_label">Primary Address</label>
                                                                            <div class="col-md-7">
                                                                                {{--<input type="text" class="form-control modal-form_input" id="primaryAddress">--}}
                                                                                {{ Form::text('address', null, ['class' => 'form-control modal-form_input']) }}
                                                                            </div>
                                                                        </div>
                                                                        {{--<div class="row">--}}
                                                                            {{--<div class="col-md-11 text-right">--}}
                                                                                {{--<button type="button" class="btn btn-outline-secondary modal-form_button mr-2" data-dismiss="modal">Close</button>--}}
                                                                                {{--<button type="button" class="btn btn-primary modal-form_button">Save changes</button>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}

                                                                    <div class="row">
                                                                        <div class="col-md-10 offset-md-1 d-flex justify-content-end">
                                                                            {{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
                                                                        </div>
                                                                    </div>
                                                                    {{ Form::close() }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <p class="profile_information">
                                                @if(is_null($user->address))
                                                    Address not set
                                                @else
                                                    {{ $user -> address }}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
