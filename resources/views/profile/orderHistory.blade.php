@extends('layout.app')
@section('content')

    <div class="container-fluid mt-5 min-main-height">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="row">
                    <h1 class="page_title">YOUR PROFILE</h1>
                </div>

                <div class="row mt-4">
                    @include('profile.profileSidebar')
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <h3 class="page_sub-title">ORDER HISTORY</h3>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-hover order-history_table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">PRODUCT ID</th>
                                        <th scope="col">IMAGE</th>
                                        <th scope="col">ORDER DATE</th>
                                        <th scope="col">DELIVERY DATE</th>
                                        <th scope="col">PRICE</th>
                                    </tr>
                                    </thead>
                                    <tbody class="table-with-image">
                                        @foreach($customerOrders as $order)
                                            @foreach($order->orderItems as $orderitem)
                                                <tr>
                                                    {{--{{dd($orderitem->product()->id)}}--}}
                                                    <td>{{ $order->id }}</td>
                                                    <td>

                                                        <img src="{{asset($orderitem->imageURI) }}"
                                                             class="img-fluid mx-auto table-image">
                                                    </td>
                                                    <td>{{ $order->created_at->todatestring() }}</td>
                                                    <td>{{ $order->created_at->addDays(2)->todatestring() }}</td>
                                                    <td>Rs. {{ $orderitem->price * $orderitem->quantity }}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-12 d-flex justify-content-center mt-5">
                                <a href="{{route('productsPage')}}" class="btn keep-shopping_button">KEEP ON SHOPPING</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection