<!-- footer -->
<div class="container-fluid mt-5 footer">
    <div class="row footer-top">
        <div class="col-md-8 offset-md-2">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 footer-top_logo">
                            <img src="../images/icons/logo-footer.svg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 footer-top_contact--details mt-4 d-flex">
                            <img src="../images/icons/phone.svg" class="d-inline-block align-self-center" alt="">
                            <p class="ml-3 d-inline-block">+977 9803531145</p>
                        </div>
                        <div class="col-md-12 footer-top_contact--details d-flex">
                            <img src="../images/icons/mail.svg" class="d-inline-block align-self-center" alt="">
                            <p class="ml-3 d-inline-block">lambada@gmail.com</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mt-4 footer-top_social-links">
                            <a href="#" class="d-inline-block">
                                <img src="../images/icons/faceboook.svg" alt="">
                            </a>
                            <a href="#" class="d-inline-block ml-2">
                                <img src="../images/icons/instagram.svg" alt="">
                            </a>
                            <a href="#" class="d-inline-block ml-2">
                                <img src="../images/icons/twitter.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <h4 class="footer-top_links--heading">USEFUL LINKS</h4>
                    <ul class="footer-top_links mt-2">
                        <li>
                            <a href="{{route('termsPage')}}">Terms & condition</a>
                        </li>
                        <li>
                            <a href="{{route('policyPage')}}">Privacy Policies</a>
                        </li>
                        <li>
                            <a href="{{route('aboutPage')}}">About Us</a>
                        </li>
                        <li>
                            <a href="{{route('contact.show')}}">Contact Us</a>
                        </li>
                        <li>
                            <a href="{{route('howToPage')}}">How To Use</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4">
                    <h4 class="footer-top_links--heading">MY ACCOUNT</h4>
                    <ul class="footer-top_links mt-2">

                        @guest
                            <li>
                                <a href="{{route('login')}}">Login</a>
                            </li>
                            <li>
                                <a href="{{route('register')}}">Register</a>
                            </li>
                        @else
                            <li>
                                <a href="{{route('user.profile', auth()->user()->id)}}">Profile</a>
                            </li>
                            <li>
                                <a href="{{route('order.index', auth()->user()->id)}}">Order History</a>
                            </li>
                            <li>
                                <a href="{{route('homePage')}}">View Cart</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row footer-middle">
        <div class="col-md-4 offset-md-2 mt-5 newletters">
            <h4 class="newletters_heading">NEWLETTERS</h4>
            <div class="input-group mt-3 newletters_email">
                <input type="text" class="form-control newletters_email--input" placeholder="Email">
                <div class="input-group-append">
                    <button class="btn newletters_email--button" type="button">
                        <svg class="mb-1" xmlns="http://www.w3.org/2000/svg" viewBox="637.193 3195.2 28.891 12"
                             height="9px">
                            <path id="input-arrow" class="cls-1"
                                  d="M8.891,11a.567.567,0,0,0-.16-.389l-5.4-5.45a.541.541,0,0,0-.771,0,.554.554,0,0,0,0,.778l4.465,4.51H-19.455A.548.548,0,0,0-20,11a.548.548,0,0,0,.545.55H7.03l-4.465,4.51a.554.554,0,0,0,0,.778.541.541,0,0,0,.771,0l5.4-5.45A.564.564,0,0,0,8.891,11Z"
                                  transform="translate(657.193 3190.2)"/>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-4 newletters_img--container">
            <img src="../images/mockUpCollection.png" class="img-fluid pull-right" alt="">
        </div>
    </div>
    <div class="row footer-bot">
        <div class="col-md-8 offset-md-2 text-center align-self-center">
            <small>&copy; 2018 Roshit Shrestha. All rights reserved</small>
        </div>
    </div>
</div>