<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../js/jquery.slim.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('#occasion_selector-navigation').on('change', function () {
            var tag = $(this).val();
            console.log(tag)
            $('#occasion_search-navigation').attr('href', tag);
        })
    });
</script>

@stack('script')
{{--<script src="../slick/slick.min.js"></script>--}}
<script src="../js/script.js"></script>

