<nav class="navbar navbar-expand-md navbar-light navigation-bar" id="navigation">
    <div class="col-md-8 offset-md-2">
        <div class="row">
            <a href="{{ route('homePage') }}" class="navbar-brand">
                <img src="../images/icons/logo-footer.svg" alt="logo">
            </a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#mainNav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="mainNav">
                <div class="navbar-nav w-100 align-items-center">
                    <div class="input-group d-flex align-self-center justify-content-center">
                        {{--<select class="occasion_select" id="">--}}
                            {{--<option selected>Choose an Occasion</option>--}}
                            {{--<option value="1">One</option>--}}
                            {{--<option value="2">Two</option>--}}
                            {{--<option value="3">Three</option>--}}
                        {{--</select>
                       --}}
                        <select class="occasion_select" id="occasion_selector-navigation">
                            <option>Select Occasion</option>
                            @if($tags)
                                @foreach($tags as $tag)
                                    <option value="{{ route('productsPage', $tag->slug) }}">{{ $tag->name }}</option>
                                @endforeach
                            @endif
                        </select>

                        <div class="input-group-append">
                            <a type="button" herf="#" class="btn occasion_select--button" id="occasion_search-navigation">GO</a>
                        </div>
                    </div>

                    <a class="navbar-item nav-link {{Request::is('/') ? "active" : "" }}" href="{{route('homePage')}}">Home</a>
                    <a class="navbar-item nav-link {{Request::is('products') ? "active" : "" }}" href="{{route('productsPage')}}">Shop</a>
                    <a class="navbar-item nav-link {{Request::is('about') ? "active" : "" }}" href="{{route('aboutPage')}}">About</a>
                    <a class="navbar-item nav-link {{Request::is('contact') ? "active" : "" }}" href="{{route('contact.show')}}">Contact</a>

                    <a class="navbar-item nav-link ml-4  d-flex align-items-center" data-toggle="collapse" href="#searchSection" role="button">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="791 176 13 13" class="header_icons">
                            <g id="search" transform="translate(791 176)">
                                <circle id="Ellipse_4" data-name="Ellipse 4" class="cls-2" cx="4.16" cy="4.16" r="4.16" transform="translate(1.3 1.04)" />
                                <line id="Line_1" data-name="Line 1" class="cls-3" x2="3.45" y2="3.45" transform="translate(8.38 8.38)" />
                            </g>
                        </svg>
                    </a>

                    {{--profile--}}
                    {{--<a class="navbar-item nav-link" href="{{ route('login') }}">--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="1270 176 13 13" class="header_icons">--}}

                            {{--<g id="user" transform="translate(1270 176)">--}}
                                {{--<path id="Path_2" data-name="Path 2" class="cls-2" d="M13.223,11.707a10.747,10.747,0,0,1-.023-1.095,2.142,2.142,0,0,0,.587-1.271c.149-.012.385-.158.453-.733a.537.537,0,0,0-.2-.537c.243-.73.747-2.99-.933-3.223a1.321,1.321,0,0,0-1.191-.457c-2.3.042-2.58,1.738-2.075,3.681a.538.538,0,0,0-.2.537c.069.575.3.72.453.733a2.138,2.138,0,0,0,.6,1.271,10.75,10.75,0,0,1-.024,1.095c-.456,1.225-3.529.881-3.671,3.243h9.88C16.738,12.587,13.678,12.931,13.223,11.707Z"--}}
                                      {{--transform="translate(-5.18 -3.249)" />--}}
                            {{--</g>--}}
                        {{--</svg>--}}

                    {{--</a>--}}

                    {{--cart--}}
                    <a class="navbar-item nav-link position-relative d-flex align-items-center" href="{{ route('cart.index') }}">
                    {{--<a class="navbar-item nav-link position-relative" href="#">--}}
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="1313 176 13 13.24" class="header_icons">
                            <g id="cart" transform="translate(1313 176)">
                                <circle id="Ellipse_1" data-name="Ellipse 1" class="cls-2" cx="0.78" cy="0.78" r="0.78" transform="translate(9.36 11.18)"
                                />
                                <circle id="Ellipse_2" data-name="Ellipse 2" class="cls-2" cx="0.78" cy="0.78" r="0.78" transform="translate(5.98 11.18)"
                                />
                                <circle id="Ellipse_3" data-name="Ellipse 3" cx="0.52" cy="0.52" r="0.52" transform="translate(0 0.52)" />
                                <path id="Path_3" data-name="Path 3" class="cls-3" d="M12.14,13.4H8.164c-.889,0-1.02-.453-1.176-1.047L4.91,3.961C4.723,3.331,4.459,3,3.733,3H2"
                                      transform="translate(-1.48 -2.22)" />
                                <path id="Path_4" data-name="Path 4" class="cls-3" d="M16,16h8.06l-1.87,4.94H17.2" transform="translate(-11.84 -11.84)" />
                            </g>
                        </svg>
                        <span class="badge cart-badge">
                            {{ Cart::count() }}
                        </span>
                    </a>


                    @guest
                        {{--profile--}}
                        <a class="navbar-item nav-link d-flex align-items-center" href="{{ route('login') }}" style="white-space: nowrap">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="1270 176 13 13" class="header_icons ml-4 mr-2">

                                <g id="user" transform="translate(1270 176)">
                                    <path id="Path_2" data-name="Path 2" class="cls-2" d="M13.223,11.707a10.747,10.747,0,0,1-.023-1.095,2.142,2.142,0,0,0,.587-1.271c.149-.012.385-.158.453-.733a.537.537,0,0,0-.2-.537c.243-.73.747-2.99-.933-3.223a1.321,1.321,0,0,0-1.191-.457c-2.3.042-2.58,1.738-2.075,3.681a.538.538,0,0,0-.2.537c.069.575.3.72.453.733a2.138,2.138,0,0,0,.6,1.271,10.75,10.75,0,0,1-.024,1.095c-.456,1.225-3.529.881-3.671,3.243h9.88C16.738,12.587,13.678,12.931,13.223,11.707Z"
                                          transform="translate(-5.18 -3.249)" />
                                </g>
                            </svg>
                            Login
                        </a>
                        {{--<a href="{{ route('login') }}">--}}
                        {{--<button>Sign In</button>--}}
                        {{--</a>--}}
                        {{--<a href="{{ route('register') }}">--}}
                        {{--<button>Join</button>--}}
                        {{--</a>--}}
                    @else
                        <li class="nav-item dropdown ml-4">
                            <a class="nav-link dropdown-toggle d-flex align-items-center" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ explode(' ', trim(Auth::user()->name))[0] }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('user.profile', auth()->user()->id) }}">Profile</a>
                                <a class="dropdown-item" href="{{ route('cart.index', auth()->user()->id) }}">My Cart</a>
                                <a class="dropdown-item" href="{{ route('order.index', auth()->user()->id) }}">Order History</a>
                                @role('admin')
                                <a class="dropdown-item" href="{{ route('admin.dashboard', auth()->user()->id) }}">Dashboard</a>
                                @endrole
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                        {{--aria-expanded="false" aria-haspopup="true">--}}
                        {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="#">Account Setting</a>--}}
                        {{--<li><a href="#">Your Activity</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="{{ route('logout') }}"--}}
                        {{--onclick="event.preventDefault();--}}
                        {{--document.getElementById('logout-form').submit();">--}}
                        {{--Logout--}}
                        {{--</a>--}}

                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST"--}}
                        {{--style="display: none;">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--</form>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                    @endguest
                </div>
                <!-- end of navbar-nav -->
            </div>
            <!-- end of collapse -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of container -->
</nav>
<div class="search_bar collapse" id="searchSection">
    <div class="container">
        {{--<input type="text" class="form-control search_bar--input" name="" value="">--}}
        {{--<button class="btn btn-primary" data-toggle="collapse" data-target="#searchSection">X</button>--}}
        <div class="input-group justify-content-center align-items-center">
            <form action="{{route('productSearch')}}" method="get" class="form-inline ml-5">
                <input type="text" name="search" class="form-control search_bar--input" placeholder="Search for...">
                <div class="input-group-append">
                    <button class="btn search_button--search" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="791 176 13 13"
                             class="search_button--search-icon">
                            <g id="search" transform="translate(791 176)">
                                <circle id="Ellipse_4" data-name="Ellipse 4" class="cls-2" cx="4.16" cy="4.16" r="4.16"
                                        transform="translate(1.3 1.04)"/>
                                <line id="Line_1" data-name="Line 1" class="cls-3" x2="3.45" y2="3.45"
                                      transform="translate(8.38 8.38)"/>
                            </g>
                        </svg>
                    </button>
                </div>
            </form>
            <button type="button" class="btn search_button--close ml-4" data-toggle="collapse"
                    data-target="#searchSection">
                <span>&times;</span>
            </button>
        </div>
    </div>
</div>
