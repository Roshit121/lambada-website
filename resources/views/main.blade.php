<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('partial._head')

<body>
<div id="app">
   @include('partial._nav')
   @yield('content')
</div>

<!-- Scripts -->
@include('partial._script')

</body>
</html>