// changing style of navigation on scroll
var scrollTop = 0;
$(window).scroll(function () {
    scrollTop = $(window).scrollTop();

    if (scrollTop >= 100) {
        // $('#logo').removeClass('brand_image--hide');
        // $('#logo-white').addClass('brand_image--hide');
        $('#navigation').addClass('navigation-bar--scrolled');
        $('#navigation').removeClass('navigation-bar');
        $('#navigation').addClass('position-fixed');
        $('#navigation_on_banner').addClass('navigation-bar--scrolled');
        $('#navigation_on_banner').removeClass('navigation-bar');
        $('#navigation_on_banner').addClass('position-fixed');
        $('#navigation_on_banner').removeClass('navbar-dark');
        $('#navigation_on_banner').addClass('navbar-light');
        $('#navigation_on_banner #logo').removeClass('brand_image--hide');
        $('#navigation_on_banner #logo-white').addClass('brand_image--hide');
        $('#navigation_on_banner #occasion_selection').removeClass('invisible');
    } else if (scrollTop < 100) {

        $('#navigation').removeClass('navigation-bar--scrolled');
        $('#navigation').addClass('navigation-bar');
        $('#navigation').removeClass('position-fixed');
        $('#navigation_on_banner').removeClass('navigation-bar--scrolled');
        $('#navigation_on_banner').addClass('navigation-bar');
        $('#navigation_on_banner').removeClass('position-fixed');
        $('#navigation_on_banner').addClass('navbar-dark');
        $('#navigation_on_banner').removeClass('navbar-light');
        $('#navigation_on_banner #logo').addClass('brand_image--hide');
        $('#navigation_on_banner #logo-white').removeClass('brand_image--hide');
        $('#navigation_on_banner #occasion_selection').addClass('invisible');
        // $('#nav').removeClass('nav-scrolled');
    }

});

var $gallery = $('.carousel-item');
var slideCount = null;
$(document).ready(function () {
    $gallery.slick({
        speed: 250,
        fade: true,
        cssEase: 'linear',
        swipe: true,
        swipeToSlide: true,
        touchThreshold: 10
    });
});

$gallery.on('init', function (event, slick) {
    slideCount = slick.slideCount;
    setSlideCount();
    setCurrentSlideNumber(slick.currentSlide);
});

$gallery.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    setCurrentSlideNumber(nextSlide);
});

function setSlideCount() {
    var $el = $('.slide-count-wrap').find('.total');
    $el.text(slideCount);
}

function setCurrentSlideNumber(currentSlide) {
    var $el = $('.slide-count-wrap').find('.current');
    $el.text(currentSlide + 1);
}

// numbering for homepage mockup slider
$(document).ready(function () {
    $('#carousel_home .slick-prev').after('<div class="slide-count-wrap">\
                                                <span class="current">1</span> / \
                                                <span class="total">3</span>\
                                            </div>');
});

// product page carousel
$(document).ready(function () {
    $('.products-slider').slick({
        // dots: true,
        arrows: false,
        // fade: true,
        // cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1200,

        speed: 100,
        fade: true,
        cssEase: 'linear'
    });

    // carousel change on hover
    function slickPause() {
        $('.products-slider').slick('slickPause');
        $(".products-slider").slick("goTo", 0);
    }

    slickPause();

    $('.product-container').mouseover(function () {
        $(this).find('.products-slider').slick('slickPlay');
        // $(this).find(".products-slider").slick("goTo", 1);
    });

    $('.product-container').mouseout(function () {
        slickPause();
    });
});
