<?php

namespace App\Http\Controllers;

use App\OrderItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserProfileController extends Controller
{
    public function showProfile(User $user)
    {
        return view('profile.profileShow', compact('user'));
    }

//    public function orderHistory()
//    {
//        $orderitems = OrderItem::all();
//        return view('profile.orderHistory', compact('orderitems'));
//    }

    public function updateUserProfile(Request $request, User $user)
    {
        $userProfileData = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
        ];
//        $data = $request->data();
        $user->update($userProfileData);
        return redirect()->route('user.profile', $user->id);
    }

    public function updateUserAddress(Request $request, User $user)
    {
        $userAddressData = [
            'address' => $request->get('address'),
        ];
        $user->update($userAddressData);
        return redirect()->route('user.profile', $user->id);
    }

    public function updateUserPassword(Request $request, User $user)
    {
        $currentPassword = $request->get('current_password');

        if (Hash::check($currentPassword, $user->password)) {

            $userData = [
                'password' => bcrypt($request->get('password'))
            ];
            $user->update($userData);
            return redirect()->back()->withSuccess('Password changed successfully');
        }
        else{
            return redirect()->back()->withWarning('Invalid current password');
        }
    }
}
