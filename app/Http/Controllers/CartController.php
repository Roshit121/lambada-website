<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{
    public function index()
    {
        $carttotal = Cart::subtotal();
        $cartitems = Cart::content();
//        dd($cartitems);
//        Session::put('url.intended', URL::full());
        return view('profile.myCart', compact('cartitems', 'carttotal'));
    }

    public function store(Request $request, Product $product)
    {
        $cartdata = [
            'id' => str_random(6),
            'name' => $product->name,
            'price' => $product->price,
            'qty' => 1,
            'options' => [
                'product_id' => $product->id,
                'product_code'=> $product->product_code,
                'imageURI' => $request->get('imageURI')
            ],

        ];


//        $oldCart = Session::has('cart') ? Session::get('cart') : null;

//        dd(session()->get('cart'));

        Cart::add($cartdata);

//        $newCart = Cart::add($cartdata);


//        $cart=Cart::content();

//        $cart = Cart::content();
//        $request->session()->put('cart', $cart);
//        $request->session()->save();
//        dd($request->session()->get('cart'));

        return redirect()->route('cart.index', auth()->user()->id);
    }

    public function update($id, Request $request){

        Cart::update($id, $request->name);

        return redirect()->route('cart.index', auth()->user()->id);
    }

    public function delete($id){
        Cart::remove($id);
        return redirect()->route('cart.index', auth()->user()->id);
    }
}
