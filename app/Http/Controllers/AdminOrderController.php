<?php

namespace App\Http\Controllers;

use App\CustomerOrder;
use Illuminate\Http\Request;

class AdminOrderController extends Controller
{
    public function getOrders() {
        $orders = CustomerOrder::all()->sortByDesc("created_at");
        return view('admin.orders.index', compact('orders'));
    }

    public function updateOrderStatus(CustomerOrder $customerOrder){
        $customerOrderStatus = [
            'status' => 'Completed',
        ];
        $customerOrder->update($customerOrderStatus);
        return redirect()->route('admin.orders');
    }

    public function viewOrderItems(CustomerOrder $customerOrder){
        $items = $customerOrder->orderItems()->get();
//        dd($items);
        return view('admin.orders.items', compact('items'));
    }
}
