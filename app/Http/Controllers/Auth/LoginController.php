<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SocialProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Laravel\Socialite\Facades\Socialite;
//use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    public function authenticated()
    {
        if(Auth::check()) {
//            if($url = session()->get('url.intended')) {
//                return redirect()->to($url);
//            }
            if(Auth::user()->hasRole(['admin'])) {
                return redirect('/admin/dashboard');
            }
            else{
                return redirect('/');
            }
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try
        {
            $socialUser = Socialite::driver($provider)->stateless()->user();
        }
        catch(\Exception $e)
        {
            dd($e);
            return redirect('/');
        }
        //check if we have logged provider
        $socialProvider = SocialProvider::where('provider_id',$socialUser->getId())->first();
        if(!$socialProvider)
        {
            //create a new user and provider
            $user = User::firstOrCreate([
                    'email' => $socialUser->getEmail(),
                    'name' => $socialUser->getName(),
                    'slug'=>str_slug($socialUser->getName()),
                    'password' => str_random(20),
                ]
            );
            $user->socialProviders()->create(
                ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );
            return redirect('/');
        }
        else{
            $user = $socialProvider->user;
            auth()->login($user);
            return redirect('/');
        }
    }
}
