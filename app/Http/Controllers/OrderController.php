<?php

namespace App\Http\Controllers;

use App\CustomerOrder;
use App\OrderItem;
use App\User;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    public function index()
    {
        $customerOrders = auth()->user()->customerOrders()->with('orderItems')->orderBy('created_at', 'desc')->get();
//        $customerOrders = auth()->user()->customerOrders()->with('orderItems')->with('product')->get();
//        $product = $customerOrders->with('product')->get();

        return view('profile.orderHistory', compact('customerOrders'));
    }

    public function store(Request $request, User $user)
    {
//        dd(auth()->user());
//        dd(auth()->user()->address);
        if (auth()->user()->address) {
            DB::transaction(function () use ($request, $user) {
                $orderitems = Cart::content();
                $finalPrice = Cart::subtotal();

                $customerorderdata = [
                    'user_id' => auth()->user()->id,
                    'charge' => $finalPrice,
                    'slug' => str_slug(str_limit(auth()->user()->name, 3, '')) . date('Ymdhis'),
                    'status' => 'On Process',

                ];
                $orderdata = [];
                $order = CustomerOrder::create($customerorderdata);
                foreach ($orderitems as $orderitem) {
                    $file = $orderitem->options->imageURI;
//                $file = base64_decode($file);
                    $name = $orderitem->name;
                    $filename = $name . '-' . time() . mt_rand() . '.png';
                    if ($file) {
//                    Storage::disk('public')->put($filename, $file);
                        Storage::disk('public')->put($filename, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file)));

                        array_push($orderdata, [
                            'order_id' => $order->id,
                            'product_id' => $orderitem->options->product_id,
                            'name' => $orderitem->name,
                            'price' => $orderitem->price,
                            'quantity' => $orderitem->qty,
//                    'imageURI' => $orderitem->options->imageURI,

//                        'imageURI' => $path,
                            'imageURI' => Storage::url($filename)
                        ]);
                    }
                }



                $order->orderItems()->insert($orderdata);

                $orderEmailData = [
                    'email' => auth()->user()->email,
                    'name'=>auth()->user()->name,
                    'ordernumber'=>count($orderdata),
                    'subject'=>'New order'
                ];

//                Mail::send('contact.orderEmail', $orderEmailData, function($message) use ($orderEmailData){
//                    $message->from($orderEmailData['email']);
//                    $message->to('roshitshrestha@gmail.com');
//                    $message->subject($orderEmailData['subject']);
//                });
            });
            Cart::destroy();


            return redirect()->route('order.index');
        } else {
            return redirect()->route('user.profile', auth()->user()->id)->with('addressCheck', 'Please add address.');
        }
    }
}


//$file = $request->file('image');
//$file = $orderitem->options->imageURI;
//$name=$orderitem->name,
//$filename = $name.'-'.time();
//if($file) {
//    Storage::disk('local')->put($filename, $file);
//}
//
//'imageURI' => $filename->storeAs('customerImage', $filename, 'public'),