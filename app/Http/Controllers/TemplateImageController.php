<?php

namespace App\Http\Controllers;

use App\Product;
use App\TemplateImage;
use Illuminate\Http\Request;

class TemplateImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templateimages = TemplateImage::all();
        return view('admin.template.index', compact('templateimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'template_image'=>'required'
        ]);
        //Template Image
        $templateFile = $request->file('template_image');
        $templateFilenameWithExt = $templateFile->getClientOriginalName();
        $templateFilename = pathinfo($templateFilenameWithExt, PATHINFO_FILENAME);
        $extension = $templateFile->getClientOriginalExtension();
        $templateFileNameToStore= $templateFilename.'_'.time().'.'.$extension;

        $templateimagedata = [
            'image' => $templateFilename,
            'path' => $templateFile->storeAs('product', $templateFileNameToStore, 'public'),
            'meta' => 'template_image',
        ];

        $templatedata = [
            'name' => $request->get('name')
        ];

        $t = TemplateImage::create($templatedata);
        $t->image()->create($templateimagedata);

        return redirect()->route('templateImages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $templateimage = TemplateImage::find($id);
        $templateimage->delete();
//        Session::flash('success', 'Tag was deleted successfully');

        return redirect()->route('templateImages.index');
    }
}
