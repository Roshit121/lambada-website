<?php

namespace App\Http\Controllers;

use App\Product;
use App\Search;
use App\Tag;
use Illuminate\Http\Request;
use Newsletter;

class PageController extends Controller
{
    public function getHome()
    {
        $featuredProducts = Product::where('featured_product', '=', 1)->take(4)->get();
        $discounts = Product::where('discount', '>', '0')->take(2)->get();
        $tags = Tag::all();
        return view('pages.home', compact('tags', 'featuredProducts', 'discounts'));
    }

    public function getHigestDiscount()
    {

    }

    public function storeNewsletter(Request $request)
    {
//        Newsletter::delete($request->user_email);
        if ( ! Newsletter::isSubscribed($request->user_email) ) {
            Newsletter::subscribePending($request->user_email);
            return redirect()->route('homePage')->with('status', 'Thanks for subscribing.');
        }

        return redirect()->route('homePage')->with('status', 'You are already subscribed.');
    }

    public function getHowTo()
    {
        return view('pages.howTo');
    }

    public function getProducts(Tag $tag)
    {
        $products = Product::all();

        return view('pages.products', compact('tag', 'products'));
    }

    public function getAbout()
    {
        return view('pages.about');
    }

    public function getTerms()
    {
        return view('pages.terms');
    }

    public function getPolicy()
    {
        return view('pages.policy');
    }

//    test
    public function getTest()
    {
        return view('test');
    }

    public function editor(Product $product)
    {
        return view('pages.editor', compact('product'));
    }

    public function editorPreview(Request $request)
    {
        $image = $request->get('imageURI');
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
        $formImage = imagecreatefromstring($data);

        $link = "images/preview_image";

        imagejpeg($formImage, "$link/output.jpg");

        // Clear the memory of the tempory image
        imagedestroy($formImage);

        exec("cylinderwarp_main -a $link/displace.png $link/output.jpg $link/mug.png $link/wrap1.png 2>&1", $out, $returnval);
        foreach ($out as $text) {
            echo "$text<br>";
        }

        exec("cylinderwarp_front -a $link/displace_front.png $link/output.jpg $link/mug_front.png $link/wrap2.png");

        exec("cylinderwarp_flip -a $link/displace_flip.png $link/output.jpg $link/mug_flip.png $link/wrap3.png");

        return response()->json([
            'image_front' => "/$link/wrap1.png",
            'image_mid' => "/$link/wrap2.png",
            'image_back' => "/$link/wrap3.png",
        ]);
    }

    public function getFeaturedProduct()
    {
        $featuredProducts = Product::where('featured_product', '=', 1)->get();
        return view('admin.featuredProduct.index', compact('featuredProducts'));
    }

    public function removeFeaturedProduct(Request $request, Product $product)
    {

        $featuredValue = [
            'featured_product' => 0,
        ];
        $product->update($featuredValue);
        return redirect()->route('featured.product');
    }

    public function getSearchProduct(Request $request)
    {
        $search = $request->get('search');
        $products=Product::latest()->search($search)->get();
        return view('pages.searchResult', compact('products', 'search'));
    }

}
