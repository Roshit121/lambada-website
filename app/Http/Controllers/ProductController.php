<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Product;
use App\Tag;
use App\TemplateImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $templates = TemplateImage::all();
        $templateArray = array();
        foreach ($templates as $template) {
//            $templateArray[$template->name] = $template->image->url;
            array_push($templateArray, asset($template->image->url));
        }
        return view('admin.product.create', compact('categories', 'tags', 'templates', 'templateArray'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        //First Product Image
        $firstFile = $request->file('first_image');
        $firstFilenameWithExt = $firstFile->getClientOriginalName();
        $firstFilename = pathinfo($firstFilenameWithExt, PATHINFO_FILENAME);
        $extension = $firstFile->getClientOriginalExtension();
        $firstFileNameToStore= $firstFilename.'_'.time().'.'.$extension;

        //Second Product Image
        $secondFile = $request->file('second_image');
        $secondFilenameWithExt = $secondFile->getClientOriginalName();
        $secondFilename = pathinfo($secondFilenameWithExt, PATHINFO_FILENAME);
        $extension = $secondFile->getClientOriginalExtension();
        $secondFileNameToStore= $secondFilename.'_'.time().'.'.$extension;

        //Third Product Image
        $thirdFile = $request->file('third_image');
        $thirdFilenameWithExt = $thirdFile->getClientOriginalName();
        $thirdFilename = pathinfo($thirdFilenameWithExt, PATHINFO_FILENAME);
        $extension = $thirdFile->getClientOriginalExtension();
        $thirdFileNameToStore= $thirdFilename.'_'.time().'.'.$extension;

        $firstimagedata = [
            'image' => $firstFilename,
            'path' => $firstFile->storeAs('product', $firstFileNameToStore, 'public'),
            'meta' => 'first_image',
        ];
        $secondimagedata = [
            'image' => $secondFilename,
            'path' => $secondFile->storeAs('product', $secondFileNameToStore, 'public'),
            'meta' => 'second_image',
        ];
        $thirdimagedata = [
            'image' => $thirdFilename,
            'path' => $thirdFile->storeAs('product', $thirdFileNameToStore, 'public'),
            'meta' => 'third_image',
        ];

    $productdata = [
        'product_code' => $request->get('product_code'),
        'name' => $request->get('name'),
        'featured_product'=>$request->has('featured_product') ? true:false,
        'price' => $request->get('price'),
        'discount' => $request->get('discount'),
        'category_id' => $request->get('category_id'),
        'slug' => str_slug($request->get('product_code')),
        'data' => $request->get('data'),
    ];

        $p = Product::create($productdata);
        $p->image()->create($firstimagedata);
        $p->image()->create($secondimagedata);
        $p->image()->create($thirdimagedata);
        $p->tags()->sync($request->get('tags'));
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $selectedcategory = array();
        foreach ($categories as $category) {
            $selectedcategory[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $selectedtag = array();
        foreach ($tags as $tag) {
            $selectedtag[$tag->id] = $tag->name;
        }

        return view('admin.product.edit', compact('product','selectedcategory','selectedtag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMovie $request
     * @param Product $movie
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request, Product $product)
    {
        $productdata=[
            'product_code'=>$request->get('product_code'),
            'featured_product'=>$request->has('featured_product') ? true:false,
            'name'=>$request->get('name'),
            'price'=>$request->get('price'),
            'discount'=>$request->get('discount'),
            'slug'=>str_slug($request->get('product_code')),
            'category_id' => $request->get('category_id'),
        ];
        $product->update($productdata);

        if (isset($request->tags)) {
            $product->tags()->sync($request->tags);
        } else {
            $product->tags()->sync(array());
        }

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product =Product::find($id);
        $product->image()->delete();
        $product->tags()->detach();
        $product->delete();

        return redirect()->route('products.index');
    }
}
