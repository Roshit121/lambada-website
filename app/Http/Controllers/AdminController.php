<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function getDashboard(){
        $customers = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->select('users.*')
                ->where('role_user.role_id', 2)
                ->count();
        $products = DB::table('products')
                ->count();
        $orders = DB::table('customer_orders')
                ->where('status', 'On Process')
                ->count();
        $earnings = DB::table('customer_orders')
                ->select(DB::raw('SUM(charge) as total_sales'))
                ->where('status', 'Completed')
                ->get();
        return view('admin.dashboard', compact('customers', 'products', 'orders', 'earnings'));
    }
}
