<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_code' => 'required|string|max:15',
            'name' => 'required|string|max:255',
            'price' => 'required|string|max:10',
            'slug' => 'string|alpha_dash|unique:products',
            'data' => 'required',
            'first_image' => 'required',
            'second_image' => 'required',
            'third_image' => 'required',
        ];
    }

}
