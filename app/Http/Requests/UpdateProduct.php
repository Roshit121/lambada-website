<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_code' => 'required|string|max:15|unique:products,name,' . $this->product->product_code,
            'name'=>'required|max:500',
            'featured_product'=>'nullable',
            'price'=>'required|string|max:10'
        ];
    }
}
