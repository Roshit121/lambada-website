<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table='tags';
    protected $fillable = [
        'name','slug',
    ];
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
