<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $fillable = [
        'product_code','name', 'featured_product', 'price', 'discount', 'category_id', 'slug', 'data',
    ];

    public function getRouteKeyName()
    {
        return 'product_code';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'product_id');
    }

    public function scopeSearch($query,$search){
        return $query->where('name','like','%'.$search.'%');
    }
}
