<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table='order_items';
    protected $fillable = [
        'product_id','order_id', 'name', 'price', 'quantity', 'imageURI'
    ];

    public function customerOrder()
    {
        return $this->belongsTo(CustomerOrder::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
