<?php

    $image = "$argv[0]";    // base64 data uri
    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
    $formImage = imagecreatefromstring($data);

    imagejpeg( $formImage, "output.jpg" );
    // Clear the memory of the tempory image
    imagedestroy( $formImage );

    $path = "$argv[1]";      // path to image

    exec("C:\cygwin64\bin\bash.exe --login  -c '/cygdrive/c/cygwin64/bin/cylinderwarp_main.sh -a $path/displace.png $path/output.jpg $path/mug.png $path/wrap1.png'");
    exec("C:\cygwin64\bin\bash.exe --login  -c '/cygdrive/c/cygwin64/bin/cylinderwarp_front.sh -a $path/displace_front.png $path/output.jpg $path/mug_front.png $path/wrap2.png");
    exec("C:\cygwin64\bin\bash.exe --login  -c '/cygdrive/c/cygwin64/bin/cylinderwarp_flip.sh -a $path/displace_flip.png $path/output.jpg $path/mug_flip.png $path/wrap3.png");
?>