<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateImage extends Model
{
    protected $table='template_images';
    protected $fillable = [
        'name',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
