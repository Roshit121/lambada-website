<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag               = new Tag();
        $tag->name         = 'Dashain';
        $tag->slug         = 'Dashain';
        $tag->save();

        $tag               = new Tag();
        $tag->name         = 'Tihar';
        $tag->slug         = 'Tihar';
        $tag->save();

        $tag               = new Tag();
        $tag->name         = 'Birthday';
        $tag->slug         = 'Birthday';
        $tag->save();

        $tag               = new Tag();
        $tag->name         = 'Mother\'s Day';
        $tag->slug         = 'Mother\'s Day';
        $tag->save();

        $tag               = new Tag();
        $tag->name         = 'Father\'s Day';
        $tag->slug         = 'Father\'s Day';
        $tag->save();
    }
}
