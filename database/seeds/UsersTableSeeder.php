<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin     = User::create([
            'name'     => 'lambadaAdmin',
            'email'    => 'lambada@gmail.com',
            'password' => bcrypt('123456'),
            'address'  => '',
            'phone'    => '98123123123'
        ]);
        $adminRole = Role::whereName('admin')->first();
        $admin->attachRole($adminRole);
    }
}
