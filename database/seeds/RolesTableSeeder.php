<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin               = new Role();
        $admin->name         = 'admin';
        $admin->description  = 'admin';
        $admin->display_name = 'Administrator';
        $admin->save();

        $customer               = new Role();
        $customer ->name         = 'customer';
        $customer ->description  = 'customer';
        $customer ->display_name = 'Customer';
        $customer ->save();
    }
}
